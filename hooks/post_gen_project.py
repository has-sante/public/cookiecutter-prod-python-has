#! /usr/bin/env python

# pylint: disable=missing-module-docstring,comparison-of-constants,missing-function-docstring

import os
import pathlib
import shutil
import subprocess

PROJECT_PATH = pathlib.Path(os.getcwd())
DOCS_PATH = PROJECT_PATH / "docs" / "source"

JUPYTER_SUPPORT = "{{cookiecutter.jupyter_notebooks_enabled}}" == "yes"
DOCKER_ENABLED = "{{cookiecutter.docker_enabled}}" == "yes"
DOCS_ENABLED = "{{cookiecutter.docs_enabled}}" == "yes"
LINTING_ENABLED = "{{cookiecutter.linting_enabled}}" == "yes"
TESTS_ENABLED = "{{cookiecutter.tests_enabled}}" == "yes"
IS_FR = "{{cookiecutter.used_language}}" == "français"


def init_git_repo():
    is_git_already = (
        subprocess.run(["git", "status"], capture_output=True, check=False).returncode
        == 0
    )

    if not is_git_already:
        subprocess.run(["git", "init"], check=True)
        subprocess.run(["git", "checkout", "-b", "main"], check=True)
        subprocess.run(
            ["git", "commit", "-m", '"First commit"', "--allow-empty"], check=True
        )
        domain = "{{ cookiecutter.remote_vcs_url }}".replace("https://", "")
        project_path = "{{ cookiecutter.remote_vcs_project_path }}" + ".git"
        git_url_repo = f"git@{domain}:{project_path}"
        subprocess.run(["git", "remote", "add", "origin", git_url_repo], check=True)


def main():
    init_git_repo()

    if not JUPYTER_SUPPORT:
        shutil.rmtree(PROJECT_PATH / "notebooks")
        shutil.rmtree(PROJECT_PATH / "reports")
        (PROJECT_PATH / "docs" / "source" / "notebooks.md").unlink()

    if not DOCKER_ENABLED:
        (PROJECT_PATH / "Dockerfile").unlink()
        (PROJECT_PATH / "docker-compose.yml").unlink()
        (PROJECT_PATH / ".dockerignore").unlink()
        (PROJECT_PATH / "bin" / "entrypoint").unlink()

    if not LINTING_ENABLED:
        (PROJECT_PATH / ".pre-commit-config.yaml").unlink()
        (PROJECT_PATH / "{{cookiecutter.package_name}}" / "py.typed").unlink()
        (PROJECT_PATH / "yamllint-config.yml").unlink()

    if not LINTING_ENABLED and not TESTS_ENABLED and not DOCS_ENABLED:
        (PROJECT_PATH / ".gitlab-ci.yml").unlink()

    if IS_FR:
        (PROJECT_PATH / "README-en.md").unlink()
        (DOCS_PATH / "usage-en.md").unlink()
        (DOCS_PATH / "changelog-en.md").unlink()
        (PROJECT_PATH / "README-fr.md").rename(PROJECT_PATH / "README.md")
        (DOCS_PATH / "usage-fr.md").rename(DOCS_PATH / "usage.md")
        (DOCS_PATH / "changelog-fr.md").rename(DOCS_PATH / "changelog.md")
    else:
        (PROJECT_PATH / "README-fr.md").unlink()
        (DOCS_PATH / "usage-fr.md").unlink()
        (DOCS_PATH / "changelog-fr.md").unlink()
        (PROJECT_PATH / "README-en.md").rename(PROJECT_PATH / "README.md")
        (DOCS_PATH / "usage-en.md").rename(DOCS_PATH / "usage.md")
        (DOCS_PATH / "changelog-en.md").rename(DOCS_PATH / "changelog.md")

    if not DOCS_ENABLED:
        shutil.rmtree(PROJECT_PATH / "docs")

    if not TESTS_ENABLED:
        shutil.rmtree(PROJECT_PATH / "tests")


if __name__ == "__main__":
    main()
