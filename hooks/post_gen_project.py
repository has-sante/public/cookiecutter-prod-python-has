#! /usr/bin/env python

import os
import pathlib
import shutil
import subprocess

PROJECT_PATH = pathlib.Path(os.getcwd())
DOCS_PATH = PROJECT_PATH / 'docs' / 'source'

jupyter_support = '{{cookiecutter.jupyter_notebooks_enabled}}' == 'yes'
docker_enabled = '{{cookiecutter.docker_enabled}}' == 'yes'
docs_enabled = '{{cookiecutter.docs_enabled}}' == 'yes'
linting_enabled = '{{cookiecutter.linting_enabled}}' == 'yes'
tests_enabled = '{{cookiecutter.tests_enabled}}' == 'yes'
is_fr = '{{cookiecutter.used_language}}' == 'français'

def init_git_repo():
    is_git_already = subprocess.run(
        ['git', 'status'],
        capture_output=True
    ).returncode == 0

    if not is_git_already:
        subprocess.run(['git', 'init'], check=True)
        subprocess.run([
            'git', 'commit', '-m', '"First commit"', '--allow-empty'
        ], check=True)

def main():
    init_git_repo()

    if not jupyter_support:
        shutil.rmtree(PROJECT_PATH / 'notebooks')
        shutil.rmtree(PROJECT_PATH / 'reports')
        (PROJECT_PATH / 'docs'/ 'source' / 'notebooks.md').unlink()

    if not docker_enabled:
        (PROJECT_PATH / 'Dockerfile').unlink()
        (PROJECT_PATH / 'docker-compose.yml').unlink()
        (PROJECT_PATH / '.dockerignore').unlink()
        (PROJECT_PATH / 'bin' / 'entrypoint').unlink()

    if not linting_enabled:
        (PROJECT_PATH / '.pre-commit-config.yaml').unlink()
        (PROJECT_PATH / '{{cookiecutter.package_name}}' / 'py.typed').unlink()

    if is_fr:
        (PROJECT_PATH / 'README-en.md').unlink()
        (DOCS_PATH / 'usage-en.md').unlink()
        (PROJECT_PATH / 'README-fr.md').rename(PROJECT_PATH / 'README.md')
        (DOCS_PATH / 'usage-fr.md').rename(DOCS_PATH / 'usage.md')
    else:
        (PROJECT_PATH / 'README-fr.md').unlink()
        (DOCS_PATH / 'usage-fr.md').unlink()
        (PROJECT_PATH / 'README-en.md').rename(PROJECT_PATH / 'README.md')
        (DOCS_PATH / 'usage-en.md').rename(DOCS_PATH / 'usage.md')

    if not docs_enabled:
        shutil.rmtree(PROJECT_PATH / 'docs')

    if not tests_enabled:
        shutil.rmtree(PROJECT_PATH / 'tests')

if __name__ == '__main__':
    main()