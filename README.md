# Cookiecutter Prod python HAS

_Une structure standard pour initier un projet de production en Python à la Haute Autorité de Santé._

### Dépendances nécessaire pour utiliser ce template cookiecutter

- Python >=3.8
- [cruft Python package](https://cruft.github.io/cruft/) >= 2.8.0:
  Cela peut être installé via `pip` ou par `conda`, selon la façon dont vous gérez vos packages Python:

```bash
$ pip install cruft
```

ou

```bash
$ conda config --add channels conda-forge
$ conda install cruft
```

### Pour démarrer un nouveau projet, lancez la commande suivante :

    cruft create https://gitlab.com/has-sante/public/cookiecutter-prod-python-has/

ou avec ssh :

    cruft create git@gitlab.com:has-sante/public/cookiecutter-prod-python-has.git

### La structure de dossier créée est la suivante

```
├── <nom packet python>     <- Code source
├── bin                     <- Executables et CLI
├── data                    <- Data eventuellement utilisée par le projet (non tracké dans git)
├── docs                    <- Documentation du projet
|
├── notebooks               <- Notebooks (`.ipynb` pour jupyter, `.Rmd` pour Rstudio)
|
├── reports                <- Rapports d'analyse
|
├── tests                   <- Tests du projet
|
├── LICENCE
├── README.md
├── Makefile                <- Expose des helpers pour gérer le projet
└── pyproject.toml          <- Fichier de requirements for reproduire l'environnement
```

## Contribuer

Les contributions sont bienvenues !

### Tester des modifications localement

Pour tester les modifications localement la démarche est la suivante:

1. Cloner le repo et créer une branche dédiée
2. Installer [cruft](https://cruft.github.io/cruft/)
3. Faire les modifications souhaitées
4. Commiter en `[WIP]`:

```shell
git add -u
git commit -m '[WIP] test amélioration cookie-cutter'
```

5. Dans un autre dossier vierge, par exemple `../tests-cookiecutter`

```shell
cd ../tests-cookiecutter
cruft create ../cookiecutter-prod-python-has --config-file ../cookiecutter-prod-python-has/test_configs/full-config.yml -y -f
```

6. Le dossier `name-of-the-project` est crée et contient le rendu du cookiecutter

> 📝 **Note**
> Comme vous pouvez le voir il y'a plusieurs configurations pré-remplies dans le dossier `test_configs`.
> Vous pouvez vous en servir pour tester plusieurs versions du cookie-cutter sans rentrer toutes les informations à la main.

## Licence

Sous licence EUPL-v1.2
