# Cookiecutter Prod python HAS

_Une structure standard pour initier un projet de production en Python à la Haute Autorité de Santé._

## Utilisation

### Dépendances nécessaire pour utiliser ce template cookiecutter

- Python >=3.11
- [cruft Python package](https://cruft.github.io/cruft/) >= 2.8.0:
  Cela peut être installé via `pip` ou par `conda`, selon la façon dont vous gérez vos packages Python:

```bash
$ pip install cruft
```

ou

```bash
$ conda config --add channels conda-forge
$ conda install cruft
```

### Pour démarrer un nouveau projet, lancez la commande suivante :

```shell
cruft create https://gitlab.has-sante.fr/has-sante/public/cookiecutter-prod-python-has/
```

ou avec ssh :

```shell
cruft create git@gitlab.has-sante.fr:has-sante/public/cookiecutter-prod-python-has.git
```

Les variables suivantes peuvent être renseignées:

- `project_name` [Requis]: Nom du projet
- `project_slug` [Optionnel]: Identifiant du projet
- `package_name` [Optionnel]: Nom du paquet Python issu du projet
- `friendly_name` [Optionnnel]: Nom du projet en langue naturelle
- `author` [Requis]: Nom et prénom de l'auteur du projet (eg: `Timothée Chehab`)
- `email` [Requis]: Email de l'auteur du projet
- `project_type` [Requis]: Type de projet.
  - Les projets de type `package` ont vocation à publier des packets python.
  - Les projets de type `project` sont des projets autres (applications, analyses etc.)
- `docker_enabled` [Requis]: Indique si le projet utilisera Docker.
  Si oui, un DockerFile et un docer-compose minimals seront créés.
- `docs_enabled` [Requis]: Indique si le projet exposera une documentation sous forme de gitlab pages.
- `jupyter_notebooks_enabled` [Requis]: Indique si le projet utilisera des notebooks jupyter.
- `linting_enabled` [Requis]: Indique si le projet utilisera des pre-commit hooks pour faire du linting.
- `tests_enabled` [Requis]: Indique si le projet utilisera des tests unitaires.
- `remote_vcs_url` [Requis]: Url du serveur GIT distant
- `remove_vcs_project_path` [Optionnel]: Chemin du projet sur le serveur GIT distant
- `project_repository_url` [Optionnel]: Url complète du projet sur le serveur GIT distant (utiliser la valeur suggérée)
- `documentation_url` [Optionnel]: Url de la documentation en ligne du projet

### La structure de dossier créée est la suivante

```
├── <nom packet python>     <- Code source
├── bin                     <- Executables et CLI
├── data                    <- Data eventuellement utilisée par le projet (non tracké dans git)
├── docs                    <- Documentation du projet
|
├── notebooks               <- Notebooks (`.ipynb` pour jupyter, `.Rmd` pour Rstudio)
|
├── reports                <- Rapports d'analyse
|
├── tests                   <- Tests du projet
|
├── LICENCE
├── README.md
├── Makefile                <- Expose des helpers pour gérer le projet
└── pyproject.toml          <- Fichier de requirements for reproduire l'environnement
```

## Contribuer

Les contributions sont bienvenues !

### Tester des modifications localement

Pour tester les modifications localement la démarche est la suivante:

1. Cloner le repo et créer une branche dédiée
2. Installer [cruft](https://cruft.github.io/cruft/)
3. Faire les modifications souhaitées
4. Commiter en `[WIP]`:

```shell
git add -u
git commit -m '[WIP] test amélioration cookie-cutter'
```

5. Dans un autre dossier vierge, par exemple `../tests-cookiecutter`

```shell
cd ../tests-cookiecutter
cruft create ../cookiecutter-prod-python-has --config-file ../cookiecutter-prod-python-has/test_configs/full-config.yml -y -f
```

6. Le dossier `name-of-the-project` est crée et contient le rendu du cookiecutter

> 📝 **Note**
> Comme vous pouvez le voir il y'a plusieurs configurations pré-remplies dans le dossier `test_configs`.
> Vous pouvez vous en servir pour tester plusieurs versions du cookie-cutter sans rentrer toutes les informations à la main.

## Licence

Sous licence EUPL-v1.2
