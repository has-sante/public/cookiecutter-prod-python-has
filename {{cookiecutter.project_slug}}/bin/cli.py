#! /usr/bin/env python
import logging

import click

from {{cookiecutter.package_name}}.constants import LOG_LEVEL
from {{cookiecutter.package_name}}.utils import hello

logging.basicConfig(
    level=logging.getLevelName(LOG_LEVEL),
    format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
)


@click.group()
@click.version_option(package_name="{{cookiecutter.package_name}}")
def cli():
    pass


@cli.command()
def main() -> None:
    """{{cookiecutter.project_slug}} Main entrypoint"""
    click.secho(hello(), fg="green")


if __name__ == "__main__":
    cli()
