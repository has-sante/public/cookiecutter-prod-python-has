#! /usr/bin/env python
import click
from dotenv import load_dotenv

from {{cookiecutter.package_name}}.utils import hello

# see `.env` for requisite environment variables
load_dotenv()


@click.group()
@click.version_option(package_name="{{cookiecutter.package_name}}")
def cli():
    pass


@cli.command()
def main() -> None:
    """{{cookiecutter.project_slug}} Main entrypoint"""
    click.secho(hello(), fg="green")


if __name__ == "__main__":
    cli()
