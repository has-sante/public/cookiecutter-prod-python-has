"""Test cases for the main module."""

from {{cookiecutter.package_name}}.utils import hello


def test_hello():
    assert hello()
