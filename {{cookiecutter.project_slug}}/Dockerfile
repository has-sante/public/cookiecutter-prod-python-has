FROM python:3.11-slim
LABEL maintainer="{{cookiecutter.author}} <{{cookiecutter.email}}>"
ENV LANG C.UTF-8

SHELL ["/bin/bash",  "-o", "pipefail", "-c"]

# Install common functionality for downstream layers/user env
# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    curl \
    git \
    make && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ARG POETRY_VERSION="1.1.6"
# Setup Poetry for Python package and dependency management
ENV POETRY_HOME=/opt/poetry \
    POETRY_VIRTUALENVS_CREATE=true
ENV PATH="${POETRY_HOME}/bin:${PATH}"
# Install `poetry` via `pip` and system `python`
RUN pip install poetry==${POETRY_VERSION} && \
    poetry --version && \
    poetry config --list

ARG USERNAME=docker_user
ARG UID=1000
ARG GID=1000

RUN groupadd --gid ${GID} ${UID} \
    && useradd --uid ${UID} --gid ${GID} -m ${USERNAME}

USER ${USERNAME}

# Install project dependencies
COPY --chown=${USERNAME}:${USERNAME} pyproject.toml poetry.lock /app/
WORKDIR /app/
RUN poetry install --only main --no-interaction --no-root && rm -rf ~/.cache/pip && rm -rf ~/.cache/pypoetry/artifacts && rm -rf ~/.cache/pypoetry/cache



# Note: delete the below if you do NOT want application files distributed with
# your container image
COPY --chown=${USERNAME}:${USERNAME} . /app/
RUN poetry install --only main --no-interaction

CMD ["/app/bin/entrypoint"]
