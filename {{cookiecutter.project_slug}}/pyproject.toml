[tool.poetry]
name = "{{cookiecutter.project_slug}}"
version = "{{cookiecutter.version}}"
description = "{{cookiecutter.friendly_name}}"
authors = ["{{cookiecutter.author}} <{{cookiecutter.email}}>"]
license = "EUPL-v1.2"
readme = "README.md"
repository = "{{ cookiecutter.project_repository_url }}"
homepage = "{{ cookiecutter.project_repository_url }}"
include = ["bin"]
keywords = []
classifiers = [
    "Intended Audience :: Developers",
    "Natural Language :: English",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: Implementation :: CPython",
    "Programming Language :: Python",
    "Topic :: Software Development :: Libraries :: Python Modules",
]

# [tool.poetry.urls]
# Changelog = "{{ cookiecutter.project_repository_url }}/releases"

[tool.poetry.dependencies]
python = "^3.8"

# Project-Specific
python-dotenv = "^0.20.0"
click = "^8.0.4"

{%- if cookiecutter.jupyter_notebooks_enabled == 'yes' %}
# Jupyter Notebook
jupyter = "^1.0.0"
matplotlib = "^3.5.1"
jupytext = "^1.13.7"
{%- endif %}

{%- if cookiecutter.docs_enabled == 'yes' %}
# Documentation
importlib-metadata = { version = "^4.11.3", optional = true }
myst-parser = { version = "^0.17.0", optional = true }
pygments = { version = "^2.11.2", optional = true }
sphinx = { version = "^4.4.0", optional = true }
sphinx-autodoc-typehints = { version = "^1.17.0", optional = true }
pydata-sphinx-theme = { version = "^0.8.0", optional = true }
sphinxcontrib-apidoc = { version = "^0.3.0", optional = true }
sphinx-click = { version = "^3.1.0", optional = true }

# see: https://github.com/sphinx-doc/sphinx/issues/10291
jinja2 = { version = "<3.1", optional = true}
{%- endif %}

[tool.poetry.dev-dependencies]

{%- if cookiecutter.tests_enabled == 'yes' %}
# Testing
pytest = "^7.1.1"
pytest-cov = "^3.0.0"
pytest-mock = "^3.7.0"
pytest-sugar = "^0.9.4"
pytest-xdist = "^2.5.0"
{%- if cookiecutter.docs_enabled == 'yes' %}
xdoctest = {extras = ["all"], version = "^0.15.10"}
{%- endif %}
{%- endif %}


{%- if cookiecutter.linting_enabled == 'yes' %}
# Linting
## Type Checking and Data Validation
mypy = "^0.942" # Static type checker
## Code formatting
black = "^22.1.0" # see: https://black.readthedocs.io/en/stable/editor_integration.html
## Code quality
isort = "^5.10.1"
pylint = "^2.13.0"
## Automation and management
pre-commit = "^2.17.0"
{%- endif %}

{%- if cookiecutter.docs_enabled == 'yes' %}
[tool.poetry.extras]
docs = [
    "importlib-metadata",
    "myst-parser",
    "pygments",
    "sphinx",
    "sphinx-autodoc-typehints",
    "pydata-sphinx-theme",
    "sphinxcontrib-apidoc",
    "sphinx-click",
    "jinja2"
]
{%- endif %}

[tool.poetry.scripts]
cli = "bin.cli:cli"

#################################################################################
# Tooling configs                                                               #
#################################################################################
{%- if cookiecutter.linting_enabled == 'yes' %}
[tool.black]
line-length = 79

[tool.coverage.run]
branch = true
concurrency = ["multiprocessing"]
parallel = true
source = ["{{cookiecutter.package_name}}"]

[tool.coverage.report]
exclude_lines =[
    "pragma: no cover",
    "raise AssertionError",
    "raise NotImplementedError",
    "if __name__ == .__main__.:",
]
fail_under = 70
show_missing = true
skip_covered = true

[tool.isort]
profile = "black"
atomic = "true"
combine_as_imports = "true"
line_length = 79

[tool.mypy]
disallow_untyped_defs = false
files = ["{{cookiecutter.package_name}}/*.py","bin/*.py"]
exclude = "notebooks/config.py"
ignore_missing_imports = true
pretty = true
show_column_numbers = true
show_error_context = true
show_error_codes = true

[tool.pylint.basic]
good-names-rgxs = ["^Test_.*$", "logger"]

[tool.pylint.messages_control]
disable = [
  # Explicitly document only as needed
  "missing-module-docstring",
  "missing-class-docstring",
  "missing-function-docstring",
  # Black & Flake8 purview
  "line-too-long",
  "bad-continuation",
  "c-extension-no-member",
{%- if cookiecutter.jupyter_notebooks_enabled == 'yes' %}
  # Ignore errors resulting from Jupyter notebook-style programming
  "invalid-name",
  "redefined-outer-name",
  "reimported",
  "ungrouped-imports",
  "wrong-import-order",
  "wrong-import-position",
{%- endif %}
]

[tool.pylint.similarities]
# Ignore imports when computing similarities.
ignore-imports = "yes"
{%- endif %}

[tool.cruft]
skip = [
    ".git",
    "README.md",
]

{%- if cookiecutter.tests_enabled == 'yes' %}
[tool.pytest.ini_options]
{%- if cookiecutter.docs_enabled == 'yes' %}
addopts = ["-rfsxX", "-l", "--tb=short", "--strict-markers", "-vv", "--xdoctest"]
{%- else %}
addopts = ["-rfsxX", "-l", "--tb=short", "--strict-markers", "-vv"]
{%- endif %}

xfail_strict = "true"
testpaths = ["tests",]
norecursedirs = [".*", "*.egg", "build", "dist",]
{%- endif %}

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
