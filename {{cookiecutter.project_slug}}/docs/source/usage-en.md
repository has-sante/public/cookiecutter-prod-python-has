# Usage

## Requirements

- TODO

{% if cookiecutter.project_type == 'package' %}

# Installation

You can install {{cookiecutter.friendly_name}} via [pip](https://pip.pypa.io/):

```shell script
pip install {{cookiecutter.project_slug}}
```

{%- endif %}

# Using the project

- TODO

## Running the project

> 📝 **Note**
> All following commands are relative to the project root directory and assume
> `make` is installed.

You can run the project as follows:

### Locally via Poetry

Run:

```shell script
make provision-environment # Note: installs ALL dependencies!
poetry shell # Activate the project's virtual environment
{%- if cookiecutter.jupyter_notebooks_enabled == 'yes' %}
jupyter notebook # Launch the Jupyter server
{%- endif %}
cli main # Run the project main entrypoint
```

{%- if cookiecutter.jupyter_notebooks_enabled == 'yes' %}

> 📝 **Note**
> If you want to launch the jupyter notebooks directly, simply use `make jupyter-notebook`.

{%- endif %}

{% if cookiecutter.docker_enabled == 'yes' %}

### Docker Container Environment

Run:

```shell script
make docker-run
```

To build a docker image and run the `bin/entrypoint` file within the corresponding container.
{%- if cookiecutter.jupyter_notebooks_enabled == 'yes' %}

To run the jupyter notebook, you can use:

```shell script
make docker-jupyter-run
```

You can then navigate to the Jupyter notebook URL displayed on your console
{%- endif %}.
{%- endif %}

# Development

> 📝 **Note**
> For convenience, many of the below processes are abstracted away
> and encapsulated in single [Make](https://www.gnu.org/software/make/) targets.

> 🔥 **Tip**
> Invoking `make` without any arguments will display
> auto-generated documentation on available commands.

## Package and Dependencies Installation

Make sure you have Python 3.11+ and [poetry](https://python-poetry.org/)
installed and configured.

To install the package and all dev dependencies, run:

```shell script
make provision-environment
```

> 🔥 **Tip**
> Invoking the above without `poetry` installed will emit a
> helpful error message letting you know how you can install poetry.

{% if cookiecutter.docker_enabled == 'yes' %}

## Docker Container Image Building/Deployment Orchestration

---

The following set of `make` targets orchestrate the project's container image
build and deploy steps:

```shell
docker-build        Builds the project's containers
docker-rm           Stops and deletes the containers
{%- if cookiecutter.jupyter_notebooks_enabled == 'yes' %}
docker-jupyter-run  Runs the jupyter notebooks inside docker
{%- endif %}
docker-run          Runs the Dockerfile with the default entrypoint
docker-run-interactive Runs the Dockerfile with a bash entrypoint
```

Note that the project's container image is insulated from the implementation
details of the application's top-level setup and execution logic which falls
under the purview of the project's entrypoint script. As such, Dockerfile
modifications will generally only be necessary when updating non-Python
environment dependencies (Python dependency updates are automatically reflected
in new image builds via the `pyproject.toml` and `poetry.lock` files).
{%- endif %}

{% if cookiecutter.tests_enabled == 'yes' %}

## Testing

We use [pytest](https://pytest.readthedocs.io/) for our testing framework.

To invoke the tests, run:

```shell script
make test
```

{%- endif %}

{% if cookiecutter.linting_enabled == 'yes' %}

## Code Quality

We use [pre-commit](https://pre-commit.com/) for our code quality
static analysis automation and management framework.

To invoke the analyses and auto-formatting over all version-controlled files, run:

```shell script
make lint
```

> 🚨 **Danger**
> CI will fail if either testing or code quality fail,
> so it is recommended to automatically run the above locally
> prior to every commit that is pushed.

### Automate via Git Pre-Commit Hooks

To automatically run code quality validation on every commit (over to-be-committed
files), run:

```shell script
make install-pre-commit-hooks
```

> ⚠️ Warning !
> This will prevent commits if any single pre-commit hook fails
> (unless it is allowed to fail)
> or a file is modified by an auto-formatting job;
> in the latter case, you may simply repeat the commit and it should pass.
> {%- endif %}

{% if cookiecutter.docs_enabled == 'yes' %}

## Documentation

```shell script
make docs-clean docs-html
```

> 📝 **Note**
> This command will generate html files in `docs/_build/html`.
> The home page is the `docs/_build/html/index.html` file.

{%- endif %}
