# Usage

## Requirements

- TODO

{% if cookiecutter.project_type == 'package' %}

# Installation

Vous pouvez {{cookiecutter.friendly_name}} via [pip](https://pip.pypa.io/):

```shell script
pip install {{cookiecutter.project_slug}}
```

{%- endif %}

# Utilisation

- TODO

## Execution du projet

> 📝 **Note**
> Toutes les commandes qui suivent sont exécutées à la _racine_ du projet.
> On suppose que `make` est installé sur le système

On peut executer ce projet ainsi:

### Localement en utilisant poetry

Par exemple :

```shell script
make provision-environment # Note: installe toutes les dépendances !
poetry shell # Active le virtualenv du projet
{%- if cookiecutter.jupyter_notebooks_enabled == 'yes' %}
jupyter notebook # lance le serveur jupyter
{%- endif %}
cli main # Utilise la CLI du projet
```

{%- if cookiecutter.jupyter_notebooks_enabled == 'yes' %}

> 📝 **Note**
> Si vous voulez simplement lancer les notebooks jupyter, utilisez `make jupyter-notebook`.

{%- endif %}

{% if cookiecutter.docker_enabled == 'yes' %}

### En utilisant Docker

Exécutez :

```shell script
make docker-run
```

Cette commande va construire l'image docker et executer (dans le container docker) l'entrypoint situé dans `bin/entrypoint`.
{%- if cookiecutter.jupyter_notebooks_enabled == 'yes' %}

Pour lancer les notebooks jupyter dans docker, utilisez:

```shell script
make docker-jupyter-run
```

Vous pouvez ensuite ouvrir l'url affichée dans votre terminal.
{%- endif %}.
{%- endif %}

# Développement

> 📝 **Note**
> Une partie des étapes ci-dessous, sont encapsulées dans le `Makefile` pour faciliter leur utilisation.

> 🔥 **Tip**
> En invoquant `make` sans aucun argument, une liste des commandes disponibles sera affichée.

## Packets et gestion des dépendances

Assurez-vous d'avoir `Python 3.11` et [poetry](https://python-poetry.org/) installés configurés.

Pour installer le projet ainsi que toutes les dépendances (incluant celles de dev):

```shell script
make provision-environment
```

> 🔥 **Tip**
> Exécuter cette commande sans poetry affichera un message d'erreur utile, vous indiquant de l'installer.

{% if cookiecutter.docker_enabled == 'yes' %}

## Utilisation de Docker

Les commandes `make` suivantes permettent d'utiliser le projet avec docker facilement:

```shell
docker-build            Build les containers du projet
docker-rm               Arrête et supprime les containers du projet
{%- if cookiecutter.jupyter_notebooks_enabled == 'yes' %}
docker-jupyter-run      Exécute les jupyter notebooks dans docker
{%- endif %}
docker-run              Exécute les containers docker avec leur entrypoint par défaut
docker-run-interactive  Exécute les containers docker avec un terminal interactif
```

Le Dockerfile a été conçu pour être peu couplée aux détails du projet en lui même.
En utilisant des entrypoints différents, il est possible de changer le comportement des containers sans modifier le Dockerfile en lui même.

{%- endif %}

{% if cookiecutter.tests_enabled == 'yes' %}

## Tests

On utilise [pytest](https://pytest.readthedocs.io/) pour les tests unitaires.

Pour lancer les tests, utilisez :

```shell script
make test
```

{%- endif %}

{% if cookiecutter.linting_enabled == 'yes' %}

## Qualité du code

On utilise [pre-commit](https://pre-commit.com/) pour orchestrer les différentes étapes de linting.

Pour lancer le linting, utilisez :

```shell script
make lint
```

> 🚨 **Danger**
> La CI cassera si les tests ou le linting ne passent pas.
> Il est donc recommandé d'executer les checks avant de pousser les changements.

### Automatisation avec les pre-commit-hooks

Pour lancer le linting avant chaque commit, utilisez:

```shell script
make install-pre-commit-hooks
```

> ⚠️ Attention !
> Si les vérifications ne passent pas, cela empêchera le commit.
> Certains checks peuvent modifier automatiquement des fichiers (`isort` et `black` notamment).
> Dans ce cas, il suffit de stagé les changements et de refaire `git commit`

{%- endif %}

{% if cookiecutter.docs_enabled == 'yes' %}

## Documentation

Pour générer la documentation, utilisez :

```shell script
make docs-clean docs-html
```

> 📝 **Note**
> Cette commande va générer des fichiers html dans `docs/_build/html`.
> La page d'accueil de la doc est `docs/_build/html/index.html`.

{%- endif %}
