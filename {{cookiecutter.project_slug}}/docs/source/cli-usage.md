# CLI Usage

{{cookiecutter.friendly_name}}.

**Usage**:

```{eval-rst}
.. click:: bin.cli:cli
    :prog: cli
    :nested: full
```
