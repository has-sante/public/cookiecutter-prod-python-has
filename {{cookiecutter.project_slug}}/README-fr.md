# {{cookiecutter.project_name}}

> 📝 **Note**
> Utilisez le français pour tout le contenu de ce projet

---

## Description

> 📝 **Explication du contenu à rédiger**
>
> Cette partie présente brièvement le contexte général du projet.
> Elle contient les éléments suivants dans un ou plusieurs paragraphes :
>
> - Objectif du projet
> - Livrables : rapport, données, application, etc.
> - Public visé
> - Contexte historique (optionnel)
>
> - Acteurs impliqués : services, externes, etc.
> - Temporalité du projet : date de démarrage, autres dates marquantes
>
> Optionnel : une image qui permet de visualiser le résultat du projet
> Ex :
>
> - capture d'écran d'un tableau de bord ou d'une application
> - figure marquante d'un rapport, etc.

## Liens utiles

- Documentation : [{{cookiecutter.documentation_url}}]({{cookiecutter.documentation_url}})
- Code source : [{{ cookiecutter.project_repository_url }}]({{ cookiecutter.project_repository_url }})

> 📝 **Explication du contenu à rédiger**
>
> Vous pouvez aussi rajouter d'autres liens utiles : protocole, rapport d'étude, lien site internet, tutoriels, guide utilisateur

## Contexte technique

### Données utilisées

> 📝 **Explication du contenu à rédiger**
>
> Décrire les données utilisées et leur éventuelles restrictions d'accès.

### Schéma flux de données (optionnel)

### Technologies

> 📝 **Explication du contenu à rédiger**
>
> Donner quelques repères sur les technologies utilisées : langage, logiciels, éventuellement dossiers où trouver le code, etc.

### Maintenance

> 📝 **Explication du contenu à rédiger**
>
> Expliquer si le projet est maintenu et comment, par exemple parmi les alternatives suivantes :
>
> - Projet en développement actif
> - Projet finalisé ✅, sans maintenance ni modification
> - Maintenance minimale 📞, pour mettre à jour les dépendances ou en cas de problème
> - Maintenance planifiée ⏰, avec des tâches récurrentes à réaliser (auquel cas décrire les tâches et leur fréquence)

## Contacts

> 📝 **Explication du contenu à rédiger**
>
> Description des contacts importants du projet, en précisant pour chacun :
>
> - l'organisation (service, pôle, etc),
> - le rôle sur le projet,
> - le contact mail (générique et non personnel).

## Détails techniques (optionnel)

> 📝 **Explication du contenu à rédiger**
>
> Section optionnelle, à utiliser lorsque le README est la seule page de documentation, pour alléger la partie "Contexte technique".
>
> Par exemple, si les informations sont trop conséquentes mettre ici :
>
> - le détail de la description des données
> - le flux de données, etc.
